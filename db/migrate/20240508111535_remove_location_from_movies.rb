class RemoveLocationFromMovies < ActiveRecord::Migration[7.1]
  def change
    remove_reference :movies, :location, null: false, foreign_key: true
  end
end
