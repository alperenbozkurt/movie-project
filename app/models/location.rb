class Location < ApplicationRecord
  has_many :movies

  def to_s
    [name, country].join('-') 
  end
end
