class DashboardController < ApplicationController
  def index
    @movies = Movie.select('movies.*, AVG(reviews.star) AS average_star')
                  .joins(:reviews)
                  .group('movies.id')
                  .order('average_star DESC')
                  .includes(:actors, :locations, :director)

  end

  def search
    @actors = Actor.where("name ILIKE ?", "%#{params[:actor_name]}%")
                   .includes(:movies)

  end
end
