require 'csv'

class ImportMoviesJob < ApplicationJob
  queue_as :default

  BATCH_SIZE = 100

  def perform(file_path)
    csv_text = File.read(file_path)
    csv = CSV.parse(csv_text, headers: true)

    csv.each_slice(BATCH_SIZE) do |batch|
      batch.each_with_index do |row|
        process_row(row)
      end
    end
  end

  private

  def process_row(row)
    ActiveRecord::Base.transaction do
      movie_attrs = row.to_hash
      
      director_name = row['Director']
      director = Director.find_or_create_by(name: director_name)

      movie = Movie.find_or_create_by({
        name: row['Movie'],
        description: row['Description'],
        year: row['Year'],
        director: director
      })

      location = Location.find_or_create_by({
        name: row['Filming location'],
        country: row['Country']
      })
      movie.locations << location unless movie.locations.include?(location)

      actor_name = row['Actor']
      actor = Actor.find_or_create_by(name: actor_name)
      movie.actors << actor unless movie.actors.include?(actor)
    end
    logger.info "Movie created/edited successfully #{row['Movie']}"
  rescue StandardError => e
    debugger
    log_error(row['Movie'], e.message)
  end

  def log_error(movie_name, error_message)
    logger.error "Error processing movie #{movie_name}: #{error_message}"
  end
end