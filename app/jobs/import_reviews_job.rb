require 'csv'

class ImportReviewsJob < ApplicationJob
  queue_as :default

  BATCH_SIZE = 100

  def perform(file_path)
    csv_text = File.read(file_path)
    csv = CSV.parse(csv_text, headers: true)

    reviews_to_insert = []

    csv.each_slice(BATCH_SIZE) do |batch|
      batch.each do |row|
        process_row(row, reviews_to_insert)
      end
    end

    insert_reviews(reviews_to_insert)
  end

  private

  def process_row(row, reviews_to_insert)
    movie_name = row['Movie']
    user_name = row['User']
    stars = row['Stars']
    review_text = row['Review']

    movie_id = Movie.find_by(name: movie_name)&.id
    user_id = User.find_or_create_by(name: user_name)&.id
    
    return log_error(row) if movie_id.nil?

    reviews_to_insert << {
      movie_id: movie_id,
      user_id: user_id,
      star: stars,
      review: review_text
    }
  end

  def insert_reviews(reviews_to_insert)
    return if reviews_to_insert.empty?
    
    inserted_reviews = []
    inserted_ids = []
    Review.transaction do
      inserted_ids = Review.insert_all(reviews_to_insert, returning: [:id])
  
      reviews_to_insert.each_with_index do |review, index|
        inserted_ids.include?(review[:id]) ? inserted_reviews << review : log_error(review)
      end
    end
  
    logger.info "#{inserted_reviews.size} reviews inserted successfully"
  end
  
  def log_error(review)
    logger.error "Error inserting review: #{review}"
  end
end