namespace :import do
  desc "Import reviews from CSV"
  task reviews: :environment do
    file_path = Rails.root.join('lib', 'seeds', 'reviews.csv')
    ImportReviewsJob.perform_now(file_path)
  end
end