namespace :import do
  desc "Import movies from CSV"
  task movies: :environment do
    file_path = Rails.root.join('lib', 'seeds', 'movies.csv')
    ImportMoviesJob.perform_now(file_path)
  end
end